require('./log');
function divide(n1,n2){
    try{
        const ans = n1/n2;
        if(ans == Infinity){
            throw  "Can't divide by zero";
        }else{
            logger.info({message:"Operation performed successfully",level:"info"});
            return ans;
        }
    }
    catch(err){
        logger.error({message:err,level:"error"});
    }
}
console.log(divide(4,2));
console.log(divide(2,0));
