const express=require("express");
const app=express();
const bodyParser=require("body-parser");

const getdata=require("./oracle");
app.use(bodyParser.json());

app.get("/select/:id", async function(req,res) {
    const id=req.params.id;
    queryStr="";
    if (id.toLowerCase() == "all"){
        queryStr=`SELECT * FROM master1`;
    }
    else{
        queryStr=`SELECT * FROM master1 where id=${id}`;
    }
    data=await getdata.getData(queryStr,"select");
    sendStr="";
    for(x of data.rows){
        sendStr += x.ID+" | "+x.NAME+" | "+x.DEPT+" <br />";
    }
    res.send(sendStr);
})

app.post("/insert/", async function(req,res){
    var id=req.body.id;
    var name=req.body.name;
    var dept=req.body.dept;
    var result = await getdata.getData(`insert into master1 (id,name,dept) values (${id},'${name}','${dept}')`,"insert");
    res.send(result);
});

app.put("/update/:id", async function(req,res) {
    const id=req.params.id;
    const body=req.body;

    var queryStr=`Update master1 set `;
    var lastObj=body[body.length -1];
    for(x of body){
        if(x != lastObj){
            queryStr += `${x.colname}='${x.value}', `;
        }
        else{
            queryStr += `${x.colname}='${x.value}' `;
        }
    }
    queryStr+=`where id=${id}`;

    data=await getdata.getData(queryStr,"update");
    res.send(data);
})

app.delete("/delete/:id", async function(req,res){
    const id=req.params.id;
    var queryStr=`delete from master1 where id=${id}`;
    data=await getdata.getData(queryStr,"delete");
    res.send(data);
});

app.listen(2005, function(){
	console.log("Server is runnning on port 2005")
})