const oracledb = require('oracledb');
oracledb.outFormat = oracledb.OUT_FORMAT_OBJECT;
oracledb.autoCommit = true;
//const mypw = ...  // set mypw to the hr schema password

async function run(queryStr,type) {
    let connection;
    try {
        connection = await oracledb.getConnection(  {
            user          : "test_demo",
            password      : "test_demo",
            connectString : "192.168.1.34:1521/orclweb"
        });
        console.log("Connected");
        if(type=="select"){
            var result = await connection.execute(queryStr);
            return result;
        }
        else{
            var result = await connection.execute(queryStr);
            result = `${type} query get executed successfully ...`;
            return result;
        }
    } catch (err) {
        console.error(err);
    } finally {
        if (connection) {
            try {
                await connection.close();
            } catch (err) {
                console.error(err);
            }
        }
    }
}
//run();
module.exports.getData=run;